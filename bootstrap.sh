#!/usr/bin/bash

msg() {
    echo "#"
    echo "# $1"
    echo "#"
    echo
}

cd "$(dirname "$0")" || exit -1

msg "Install required software"

if [ ! -e /etc/apt/sources.list.d/vscode.list ]; then
    curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /tmp/packages.microsoft.gpg
    sudo install -o root -g root -m 644 /tmp/packages.microsoft.gpg /usr/share/keyrings/
    sudo sh -c 'echo "deb [arch=amd64 signed-by=/usr/share/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
fi

sudo apt update
sudo apt -y upgrade
sudo apt install -y \
    lightdm \
    awesome \
    stow \
    fish \
    fcitx-mozc \
    shellcheck \
    docker \
    postgresql \
    code

msg "Link configs by stow"

modules=(
    "awesome"
    "executables"
    "fish"
)

for m in ${modules[@]}; do
    stow -v --target="$HOME" $m -R
done

if [ ! -d "$HOME/Desktop" ]; then
    msg "Make home directory English"
    LANG=C xdg-user-dirs-update
fi
