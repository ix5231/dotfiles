-- automatically generated file. Do not edit (see /usr/share/doc/menu/html)

local awesome = awesome

Debian_menu = {}

Debian_menu["Debian_アプリケーション_アクセシビリティ"] = {
	{"Xmag","xmag"},
}
Debian_menu["Debian_アプリケーション_エディタ"] = {
	{"Xedit","xedit"},
}
Debian_menu["Debian_アプリケーション_グラフィック"] = {
	{"X Window Snapshot","xwd | xwud"},
}
Debian_menu["Debian_アプリケーション_シェル"] = {
	{"Bash", "x-terminal-emulator -e ".."/bin/bash --login"},
	{"Dash", "x-terminal-emulator -e ".."/bin/dash -i"},
	{"Sh", "x-terminal-emulator -e ".."/bin/sh --login"},
}
Debian_menu["Debian_アプリケーション_システム_システム管理"] = {
	{"Debian Task selector", "x-terminal-emulator -e ".."su-to-root -c tasksel"},
	{"Editres","editres"},
	{"Xclipboard","xclipboard"},
	{"Xfce Application Finder","xfce4-appfinder","/usr/share/pixmaps/xfce4-appfinder.xpm"},
	{"Xfontsel","xfontsel"},
	{"Xkill","xkill"},
	{"Xrefresh","xrefresh"},
}
Debian_menu["Debian_アプリケーション_システム_ハードウェア"] = {
	{"Xvidtune","xvidtune"},
}
Debian_menu["Debian_アプリケーション_システム_モニタリング"] = {
	{"Pstree", "x-terminal-emulator -e ".."/usr/bin/pstree.x11","/usr/share/pixmaps/pstree16.xpm"},
	{"Top", "x-terminal-emulator -e ".."/usr/bin/top"},
	{"Xconsole","xconsole -file /dev/xconsole"},
	{"Xev","x-terminal-emulator -e xev"},
	{"Xload","xload"},
}
Debian_menu["Debian_アプリケーション_システム"] = {
	{ "システム管理", Debian_menu["Debian_アプリケーション_システム_システム管理"] },
	{ "ハードウェア", Debian_menu["Debian_アプリケーション_システム_ハードウェア"] },
	{ "モニタリング", Debian_menu["Debian_アプリケーション_システム_モニタリング"] },
}
Debian_menu["Debian_アプリケーション_ネットワーク_コミュニケーション"] = {
	{"Telnet", "x-terminal-emulator -e ".."/usr/bin/telnet.netkit"},
	{"Xbiff","xbiff"},
}
Debian_menu["Debian_アプリケーション_ネットワーク"] = {
	{ "コミュニケーション", Debian_menu["Debian_アプリケーション_ネットワーク_コミュニケーション"] },
}
Debian_menu["Debian_アプリケーション_ビュワー"] = {
	{"Xditview","xditview"},
}
Debian_menu["Debian_アプリケーション_科学_数学"] = {
	{"Xcalc","xcalc"},
}
Debian_menu["Debian_アプリケーション_科学"] = {
	{ "数学", Debian_menu["Debian_アプリケーション_科学_数学"] },
}
Debian_menu["Debian_アプリケーション"] = {
	{ "アクセシビリティ", Debian_menu["Debian_アプリケーション_アクセシビリティ"] },
	{ "エディタ", Debian_menu["Debian_アプリケーション_エディタ"] },
	{ "グラフィック", Debian_menu["Debian_アプリケーション_グラフィック"] },
	{ "シェル", Debian_menu["Debian_アプリケーション_シェル"] },
	{ "システム", Debian_menu["Debian_アプリケーション_システム"] },
	{ "ネットワーク", Debian_menu["Debian_アプリケーション_ネットワーク"] },
	{ "ビュワー", Debian_menu["Debian_アプリケーション_ビュワー"] },
	{ "科学", Debian_menu["Debian_アプリケーション_科学"] },
}
Debian_menu["Debian_ウィンドウマネージャ"] = {
	{"awesome",function () awesome.exec("/usr/bin/awesome") end,"/usr/share/pixmaps/awesome.xpm"},
}
Debian_menu["Debian_ゲーム_おもちゃ"] = {
	{"Oclock","oclock"},
	{"Xclock (analog)","xclock -analog"},
	{"Xclock (digital)","xclock -digital -update 1"},
	{"Xeyes","xeyes"},
	{"Xlogo","xlogo"},
}
Debian_menu["Debian_ゲーム"] = {
	{ "おもちゃ", Debian_menu["Debian_ゲーム_おもちゃ"] },
}
Debian_menu["Debian_ヘルプ"] = {
	{"Xman","xman"},
}
Debian_menu["Debian"] = {
	{ "アプリケーション", Debian_menu["Debian_アプリケーション"] },
	{ "ウィンドウマネージャ", Debian_menu["Debian_ウィンドウマネージャ"] },
	{ "ゲーム", Debian_menu["Debian_ゲーム"] },
	{ "ヘルプ", Debian_menu["Debian_ヘルプ"] },
}

debian = { menu = { Debian_menu = Debian_menu } }
return debian