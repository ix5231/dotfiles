local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")

function make(desc)
    local contents = {
        spacing = 10,
        layout = wibox.layout.fixed.horizontal,
    }
    for k, d in ipairs(desc) do
        table.insert(contents, {
            {
                awful.widget.launcher {
                    screen = 1,
                    image = beautiful.awesome_icon,
                    command = "xterm",
                },
                height = 50,
                layout = wibox.layout.constraint,
            },
            {
                {
                    widget = wibox.widget.textbox,
                    markup = d.text,
                    align = 'center',
                },
                height = 10,
                layout = wibox.layout.constraint,
            },
            layout = wibox.layout.fixed.vertical,
        })
    end

    return {
        contents,
        left = 10,
        right = 10,
        top = 10,
        bottom = 10,
        layout = wibox.container.margin,
    }
end

local power = { make = make, }
return power